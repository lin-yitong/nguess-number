import java.util.Random;
import java.util.Scanner;

public class NGuessNumber {


    public static void main(String[] args) {
        while (true) {
            int number[] = new int[4];
            number = generateAnswer();
            System.out.println("输入1开始游戏 0退出游戏");
            Scanner scanner = new Scanner(System.in);
            int start = scanner.nextInt();
            if (start == 1) {
            } else if (start == 0) {
                System.out.println("游戏结束");
                break;
            } else {
                System.out.println("输入错误，请重新输入");
                continue;
            }
            int cs = 3;
            for (int i = 1; i <= 10; i++) {
                int Input[] = new int[4];
                Input = getPlayerInput();
                String s = compareGuessAnswer(number, Input);
                boolean b = isWin(s);
                if (b) {
                    System.out.println("回答正确");
                    break;
                } else if (cs == 0) {
                    System.out.println("您没有机会了");
                    System.out.print("正确答案是:");
                    for (int num1 = 0; num1 < 4; num1++) {
                        System.out.print(number[num1] + " ");

                    }System.out.println(" ");
                    break;
                } else {
                    if (cs-- > 0 && !b) {
                        System.out.println("还有" + cs + "次机会");
                    }
                    continue;
                }
            }
        }
    }
    public static int[] generateAnswer(){
        int a[]= new int[4];
        for(int i=0;i<4;){
            Random r= new Random();
            int c=r.nextInt(10);
            Boolean tem=true;
            for(int j=i-1; j>=0;j--){
                c=r.nextInt(10);
                if(a[j]==c){
                    tem=false;
                    continue;
                }
            }
            if(!tem){
                continue;
            }
            a[i]=c;
            i++;
        }
        return a;

    }
    public static int[] getPlayerInput(){
        int sum[]=new int[4];
        System.out.println("请输入4个数字:");
        Scanner scanner = new Scanner(System.in);
        for(int i = 0;i<4;){
            int a=scanner.nextInt();
            if(a<0||a>9){
                System.out.println("数字范围超出10请重新输入9以内的数");
                continue;
            }
            //用户输入错误
            for(int d=i-1; d>=0;d--){
                if(sum[d]==a){
                    System.out.println("数字重复了请重新输入");
                    continue;
                }
            }
            sum[i]=a;
            i++;
        }

        return sum;
    }
    public static String compareGuessAnswer(int[] answer2, int[] input2){
        int subscript=0,correct=0;
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                if(answer2[i]==input2[j]){
                    if(i==j){
                        subscript++;
                    }
                    correct++;
                }
            }
        }
        System.out.println("");
        System.out.println("正确数字有: "+correct+" 位置和数字都正确的有: "+subscript);
        String s=correct+" B,"+subscript+" A";
        return s;
    }
    public static boolean isWin(String s){
        if(s.equals("4 B,4 A")){
            return true;
        }else return false;


    }
}
