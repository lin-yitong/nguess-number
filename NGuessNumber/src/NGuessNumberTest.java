import org.junit.Test;
public class NGuessNumberTest {
    NGuessNumber cs = new NGuessNumber();
    @Test
    public void generateAnswerTest(){
        int a=0,b=0;
        int[] num = cs.generateAnswer();
        if(num.length==4){
            System.out.println("数字数量4");
        }
        for(int i=0;i<num.length;i++){
            if(num[i]>=0&&num[i]<=9){
                for (int j=i-1;j>=0;j--){
                    if(num[i]==num[j]){
                        b--;
                    }
                }
            }else{
                a--;
            }
        }
        if(a>=0&&b>=0){
            System.out.println("无重复");
        }
    }

    @Test
    public void getPlayerInputTest() {
        int[] s = cs.getPlayerInput();
        if (s.length == 4) {
            System.out.println("输入数量4");
        }
    }

    @Test
    public void compareGuessAnswerTest(){
        int []a ={1,2,3,4};
        int []b={1,2,3,4};
        String t=cs.compareGuessAnswer(a, b);
        if(!t.equals("")){
            System.out.println("判断成功");
        }
    }
    @Test
    public void isWinTest(){
        boolean b = cs.isWin("4 B,4 A");
        if(b){
            System.out.println("成功");
        }


    }
}
